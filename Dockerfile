FROM python:3.6-alpine3.9

LABEL maintainer="skostiuk@softserveinc.com"

WORKDIR /hits

COPY hits/app.py .
COPY requirements.txt .

RUN mkdir logs/
RUN touch logs/app.log
RUN apk update
RUN pip install -r requirements.txt

HEALTHCHECK --interval=2m --timeout=3s \
  CMD curl -f http://localhost/ || exit 1

EXPOSE 80
CMD ["python", "app.py"]
